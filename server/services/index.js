'use strict'

module.exports = function () {
  // Add your custom middleware here. Remember, that
  // just like Express the order matters, so error
  // handling middleware should go last.
  const app = this

  app.use('/api/locations', require('./locations'))
  app.use('/api/customers', require('./customers'))
  app.use('/api/orders', require('./orders'))

}
